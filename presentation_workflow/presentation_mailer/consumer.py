import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# from attendees.models import AccountVO, Attendee

# class AttendeeDetailEncoder(ModelEncoder):
#     model = Attendee
#     properties = [
#         "email",
#         "name",
#         "company_name",
#         "created",
#         "conference",
#     ]
#     encoders = {
#         "conference": ConferenceVODetailEncoder(),
#     }

# class AttendeeDetailEncoder(ModelEncoder):
#     model = Attendee
#     properties = [
#         "email",
#         "name",
#         "company_name",
#         "created",
#         "conference",
#     ]
#     encoders = {
#         "conference": ConferenceVODetailEncoder(),
#     }


def process_approval():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="tasks")
    channel.basic_consume(
        queue="tasks",
        on_message_callback=process_approval,
        auto_ack=True,
    )
    channel.start_consuming()


def process_rejection():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="tasks")
    channel.basic_consume(
        queue="tasks",
        on_message_callback=process_rejection,
        auto_ack=True,
    )
    channel.start_consuming()
